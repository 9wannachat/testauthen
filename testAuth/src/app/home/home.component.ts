import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { DataService } from '../_services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  loading = false;
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.loading = true;
      this.dataService.getThings().pipe(first()).subscribe(raw => {
        this.loading = false;
        console.log(raw);
      });
    
// const httpOptions = {
//   headers: new HttpHeaders({
//     "Access-Control-Expose-Headers" : "Authorization"
//     // 'Content-Type':  'application/json',
//     // 'Access-Control-Allow-Credentials': 'true',
//     //  'Access-Control-Allow-Origin': 'localhost:4200',
//     // 'Authorization': 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwb20iLCJleHAiOjE1Njk0ODUwMzUsImlhdCI6MTU2OTQ2NzAzNX0.Z0R4uLDqbIgxXoXv4JImOZg_IGdgOeL7bebbxJPt55ARkPNrlRejRW9iElZB6kqC53iPZTDDNWw371IFKszMFQ'
//   })
// };

// const httpOptions = {
//   headers: new HttpHeaders({
//     'Content-Type':  'application/json',
//     'Accept' : 'application/json, text/plain, */*'
//     // 'Authorization' : 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwb20iLCJleHAiOjE1Njk0NDE3OTAsImlhdCI6MTU2OTQyMzc5MH0.GA8kVYhJ01Vz79A-1sl4O2n4EU9zpdKaGPVU3bJQXJzZRQjWW4BoShD2PlMH61YwVUGX7HqZFC66TP36FB5z6w'
//   })
// };


// const httpOptions = new HttpHeaders().set("Authorization", "Auth_Token");

// const httpOptions = {
//   headers: new HttpHeaders({
//     'Content-Type':  'application/json',
//     'Authorization': 'my-auth-token'
//   })
// };


//     this.http.get(environment.apiUrl + '/mobile/things', httpOptions).subscribe(data => {
//       console.log(data);
//     });


  }

}
