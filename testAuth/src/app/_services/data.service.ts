import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  httpOptionsNoAuth : any;

  constructor(private http: HttpClient) {
    this.httpOptionsNoAuth = {
      headers: new HttpHeaders().set('Cache-Control', 'no-cache, no-store, must-revalidate')
    
  };
   }

  getThings(){
   return this.http.get<any>(environment.apiUrl + '/mobile/things');
  }
}
