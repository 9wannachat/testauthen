import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { sha256 } from 'js-sha256';
import { AuthService } from '../_services';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  public form : FormGroup;
  returnUrl: string;
  error = '';
  loading = false;
  submitted = false;

  constructor(private formBuilder: FormBuilder,private auth: AuthService, private router: Router) {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.auth.logout();
   }

  ngOnInit() {
  }

  submit(value) {
    this.auth.login(value.username, sha256(value.password))
    .pipe(first())
        .subscribe(
            data => {
              console.log(data)
              // this.router.navigate([this.returnUrl]);
            },
            error => {
                this.error = error;
                this.loading = false;
            });
  }

  goHome(){
     this.router.navigate(['/home']);
  }

}
