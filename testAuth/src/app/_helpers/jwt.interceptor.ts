import { HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse, HttpInterceptor } from '@angular/common/http';
import { AuthService } from '../_services';
import { Observable, throwError } from 'rxjs';
import { map, catchError, } from 'rxjs/operators';

export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let currentUser = this.authenticationService.currentUserValue;
        if (currentUser && currentUser.accessToken) {
            request = request.clone({
                setHeaders: {
                    Authorization: `${currentUser.accessToken}`,
                                     
                },
                // withCredentials: true
            });
        }
        
        console.log(request);
        return next.handle(request);
            // catchError((error: HttpErrorResponse) => {
            //     let data = {};
            //     data = {
            //         domain: error.domain,
            //         message: error.message,
            //         reason: error.reason
            //     };
            //     console.log(data);
            //     return throwError(error);
            // }));
    }
}
